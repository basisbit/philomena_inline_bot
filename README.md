# Philomena inline bot
## ENVIRNOMENT variables
- `CONFIG_NAME`  
  Name of the config, default: `default`
- `CONFIG_DIR`  
  Directory for configs, default: `/etc`

With this the default config path is: `/etc/philomena_inline_bot/default.toml`
## Config
Config with default values and comments
```toml
log_level = 'info'  # See https://docs.rs/env_logger/0.7.1/env_logger/#enabling-logging
telegram_token = ''  # The Telegram Bot Token you get from t.me/BotFather
telegram_owner_name = ''  # Your Telegram user name, so users have someone to contact
git_url = 'https://gitlab.com/TilCreator/philomena_inline_bot'  # Link to the repo (used in the about msg)
philomena_url = ''  # Link to the Philomena image board (path will be ignored, only protocol and domain are important)
default_filter = 1  # Default filter to be used, can be set by each user for themselves
placeholder_image_url = 'https://upload.wikimedia.org/wikipedia/commons/c/ca/1x1.png'  # Image used when the bot reports an error as an inline query result
cache_time = 300  # How long the bot caches queries
telegram_cache_time = 300  # How long Telegram caches inline query answers
play_button_image_url = 'https://image.freepik.com/free-icon/play-button-ios-7-interface-symbol_318-34393.jpg'  # Image that is used as thumbnail for videos. Must be a jpg
video_file_name = 'large.mp4'  # File name that is used for videos, gets joined to the path of the other versions of the image. One of "large.mp4", "medium.mp4", "small.mp4"
db_path = '/tmp/philomenia_inline_bot_default_db'  # Path where the db is located, the db is used to save the filter_id set by each user
```
## Telegram bot info template
- Name: 
  > \<site name\> inline search bot
- Description:
  > Bot to search stuff on \<site domain\> inline
- About:
  > Master: \<your TG name\> / Source https://gitlab.com/TilCreator/philomena_inline_bot / Bot pic \<link to post\>
- Commands:
  > start - Displays info about the bot any how to use it
    filter_id - Used to set your default filter id
    forget_me - Will remove any data the bot saved about you