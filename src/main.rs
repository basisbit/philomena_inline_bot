#![feature(async_closure)]

use bytes::buf::ext::BufExt;
use failure::Error;
use futures::StreamExt;
use governor::{Quota, RateLimiter};
use hyper::Client;
use hyper_tls::HttpsConnector;
use nonzero_ext::*;
use regex::Regex;
use std::{
    collections::HashMap,
    convert::{TryFrom, TryInto},
    env,
    hash::{Hash, Hasher},
    path::Path,
    sync::Arc,
};
use telegram_bot::*;
use tokio::{
    spawn,
    sync::RwLock,
    time::{delay_for, timeout, Duration},
};
use url::Url;
#[macro_use]
extern crate log;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate failure;

const APP_NAME: &str = env!("CARGO_PKG_NAME");
const CONFIG_DIR_DEFAULT: &str = "/etc"; // .join(APP_NAME)
const CONFIG_NAME_DEFAULT: &str = "default";

#[derive(Debug, Serialize, Deserialize)]
struct Config {
    log_level: String,
    telegram_token: String,
    telegram_owner_name: String,
    git_url: String,
    philomena_url: String,
    default_filter: u64,
    placeholder_image_url: String,
    cache_time: u64,
    telegram_cache_time: u64,
    play_button_image_url: String, // Has to be a jpg
    video_file_name: String,       // large.mp4, medium.mp4, small.mp4
    db_path: String,
}

impl Default for Config {
    fn default() -> Self {
        Config {
            log_level: String::from("warn"),
            telegram_token: String::from(""),
            telegram_owner_name: String::from(""),
            git_url: String::from("https://gitlab.com/TilCreator/philomena_inline_bot"),
            philomena_url: String::from(""),
            default_filter: 1,
            placeholder_image_url: String::from(
                "https://upload.wikimedia.org/wikipedia/commons/c/ca/1x1.png",
            ),
            cache_time: 300,
            telegram_cache_time: 300,
            play_button_image_url: String::from("https://image.freepik.com/free-icon/play-button-ios-7-interface-symbol_318-34393.jpg"),
            video_file_name: String::from("large.mp4"),
            db_path: String::from("/tmp/philomenia_inline_bot_default_db"),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
struct ImagesResponse {
    images: Vec<ImageResponse>,
    //total: u64,
}

#[derive(Debug, Serialize, Deserialize)]
struct ImageResponse {
    //animated: bool,                  // Whether the image is animated.
    //aspect_ratio: f64,               // The image's width divided by its height.
    //comment_count: u64,              // The number of comments made on the image.
    //created_at: String,              // The creation time, in UTC, of the image.
    //deletion_reason: Option<String>, // The hide reason for the image, or null if none provided. This will only have a value on images which are deleted for a rule violation.
    description: String, // The image's description.
    //downvotes: u64,                  // The number of downvotes the image has.
    //duplicate_of: Option<u64>, // The ID of the target image, or null if none provided. This will only have a value on images which are merged into another image.
    duration: Option<f64>, // The number of seconds the image lasts, if animated, otherwise .04.
    //faves: u64,                // The number of faves the image has.
    //first_seen_at: String, // The time, in UTC, the image was first seen (before any duplicate merging).
    format: String, // The file extension of the image. One of "gif", "jpg", "jpeg", "png", "svg", "webm".
    height: i64,    // The image's height, in pixels.
    //hidden_from_users: bool, // Whether the image is hidden. An image is hidden if it is merged or deleted for a rule violation.
    id: u64, // The image's ID.
    // intensities 	Object 	Optional object of internal image intensity data for deduplication purposes. May be null if intensities have not yet been generated.
    mime_type: String, // The MIME type of this image. One of "image/gif", "image/jpeg", "image/png", "image/svg+xml", "video/webm".
    name: String,      // The filename that the image was uploaded with.
    //orig_sha512_hash: String, // The SHA512 hash of the image as it was originally uploaded.
    //processed: bool,   // Whether the image has finished optimization.
    representations: ImageRepresentations, // A mapping of representation names to their respective URLs. Contains the keys "full", "large", "medium", "small", "tall", "thumb", "thumb_small", "thumb_tiny".
    //score: i64, // The image's number of upvotes minus the image's number of downvotes.
    //sha512_hash: String, // The SHA512 hash of this image after it has been processed.
    //size: u64,  // The number of bytes the image's file contains.
    //source_url: Option<String>, // The current source URL of the image.
    //spoilered: bool, // Whether the image is hit by the current filter.
    //tag_count: u64, // The number of tags present on the image.
    //tag_ids: Vec<u64>, // A list of tag IDs the image contains.
    tags: Vec<String>, // A list of tag names the image contains.
    //thumbnails_generated: bool, // Whether the image has finished thumbnail generation. Do not attempt to load images from view_url or representations if this is false.
    //updated_at: String,         // The time, in UTC, the image was last updated.
    //uploader: Option<String>,   // The image's uploader.
    //uploader_id: Option<u64>,   // The ID of the image's uploader. null if uploaded anonymously.
    //upvotes: u64,               // The image's number of upvotes.
    //view_url: String,           // The image's view URL, including tags.
    width: i64, // The image's width, in pixels.
                //wilson_score: f64, // The lower bound of the Wilson score interval for the image, based on its upvotes and downvotes, given a z-score corresponding to a confidence of 99.5%.
}

#[derive(Debug, Serialize, Deserialize)]
struct ImageRepresentations {
    //full: String,
    large: String,
    medium: String,
    //small: String,
    //tall: String,
    thumb: String,
    //thumb_small: String,
    //thumb_tiny: String,
}

#[derive(Debug)]
struct Query {
    orginal_query: String,
    query: String,
    page: u64,
    filter_id: u64,
    sorting_field: SortingField,
    sorting_direction: SortingDirection,
    included_bot_params: IncludedBotParams,
}

#[derive(Debug, Hash, PartialEq, Eq)]
enum SortingDirection {
    Ascending,  // asc
    Descending, // desc
}

impl SortingDirection {
    fn as_string(&self) -> String {
        match self {
            SortingDirection::Ascending => String::from("asc"),
            SortingDirection::Descending => String::from("desc"),
        }
    }
}

#[derive(Debug, Hash, PartialEq, Eq)]
enum SortingField {
    Id,              // id
    UpdatedAt,       // updated_at
    FirstSeenAt,     // first_seen_at
    AspectRatio,     // aspect_ratio
    Faves,           // faves
    Score,           // score
    _Score,          // _score
    Width,           // width
    Height,          // height
    CommentCount,    // comment_count
    TagCount,        // tag_count
    Pixels,          // pixels
    Size,            // size
    Duration,        // duration
    Random,          // random
    RandomSeed(u64), // random:<int>
}

impl SortingField {
    fn as_string(&self) -> String {
        match self {
            SortingField::Id => String::from("id"),
            SortingField::UpdatedAt => String::from("updated_at"),
            SortingField::FirstSeenAt => String::from("first_seen_at"),
            SortingField::AspectRatio => String::from("aspect_ratio"),
            SortingField::Faves => String::from("faves"),
            SortingField::Score => String::from("score"),
            SortingField::_Score => String::from("_score"),
            SortingField::Width => String::from("width"),
            SortingField::Height => String::from("height"),
            SortingField::CommentCount => String::from("comment_count"),
            SortingField::TagCount => String::from("tag_count"),
            SortingField::Pixels => String::from("pixels"),
            SortingField::Size => String::from("size"),
            SortingField::Duration => String::from("duration"),
            SortingField::Random => String::from("random"),
            SortingField::RandomSeed(seed) => format!("random:{}", seed),
        }
    }
}

#[derive(Debug)]
struct IncludedBotParams {
    page: bool,
    filter_id: bool,
    sort: bool,
}

impl Default for Query {
    fn default() -> Self {
        Query {
            orginal_query: String::from(""),
            query: String::from("*"),
            page: 1,
            filter_id: 1,
            sorting_field: SortingField::Id,
            sorting_direction: SortingDirection::Descending,
            included_bot_params: IncludedBotParams {
                page: false,
                filter_id: false,
                sort: false,
            },
        }
    }
}

// Ignores included_bot_params and orginal_query
impl PartialEq for Query {
    fn eq(&self, other: &Self) -> bool {
        self.query == other.query
            && self.page == other.page
            && self.filter_id == other.filter_id
            && self.sorting_field == other.sorting_field
            && self.sorting_direction == other.sorting_direction
    }
}

impl Eq for Query {}

// Ignores included_bot_params and orginal_query
impl Hash for Query {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.query.hash(state);
        self.page.hash(state);
        self.filter_id.hash(state);
        self.sorting_field.hash(state);
        self.sorting_direction.hash(state);
    }
}

impl Query {
    fn parse(orginal_query: &str, offset: &str, filter_id: Option<u64>) -> Result<Self, Error> {
        let mut query = Query::default();

        query.orginal_query = String::from(orginal_query);

        // Extract bot_params
        let re_bot_params = Regex::new(r"^bot_params:\s*\{(.*)\}\s*(?:,|$)\s*").unwrap();
        let re_bot_params_page = Regex::new(r"^\s*page:\s*([0-9]+)\s*$").unwrap();
        let re_bot_params_filter_id = Regex::new(r"^\s*filter_id:\s*([0-9]+)\s*$").unwrap();
        let re_bot_params_sort = Regex::new(r"^\s*sort:\s*(-?)\s*(id|updated_at|first_seen_at|aspect_ratio|faves|score|_score|width|height|comment_count|tag_count|pixels|size|duration|random:([0-9]+)|random)\s*$").unwrap();
        let re_offset_page = Regex::new(r"^page:([0-9]+)_*$").unwrap();

        let mut bot_params = Query::default();

        if &query.orginal_query != "" {
            // Keep default query when the orginal_query is empty
            if let Some(bot_params_raw) = re_bot_params.captures(&query.orginal_query) {
                for bot_param_raw in bot_params_raw.get(1).unwrap().as_str().split(",") {
                    if let Some(page) = re_bot_params_page.captures(&bot_param_raw) {
                        bot_params.page = page.get(1).unwrap().as_str().parse::<u64>()?;

                        query.included_bot_params.page = true;
                    }
                    if let Some(filter_id) = re_bot_params_filter_id.captures(&bot_param_raw) {
                        bot_params.filter_id = filter_id.get(1).unwrap().as_str().parse::<u64>()?;

                        query.included_bot_params.filter_id = true;
                    }
                    if let Some(sort) = re_bot_params_sort.captures(&bot_param_raw) {
                        bot_params.sorting_direction = match sort.get(1).unwrap().as_str() {
                            "" => SortingDirection::Descending,
                            "-" => SortingDirection::Ascending,
                            _ => panic!("Congrats, you broke the regex"),
                        };

                        let sorting_field_raw = sort.get(2).unwrap().as_str();
                        bot_params.sorting_field = match sorting_field_raw {
                            "id" => SortingField::Id,
                            "updated_at" => SortingField::UpdatedAt,
                            "first_seen_at" => SortingField::FirstSeenAt,
                            "aspect_ratio" => SortingField::AspectRatio,
                            "faves" => SortingField::Faves,
                            "score" => SortingField::Score,
                            "_score" => SortingField::_Score,
                            "width" => SortingField::Width,
                            "height" => SortingField::Height,
                            "comment_count" => SortingField::CommentCount,
                            "tag_count" => SortingField::TagCount,
                            "pixels" => SortingField::Pixels,
                            "size" => SortingField::Size,
                            "duration" => SortingField::Duration,
                            "random" => SortingField::Random,
                            _ => {
                                if sorting_field_raw.starts_with("random:") {
                                    SortingField::RandomSeed(
                                        sort.get(3).unwrap().as_str().parse::<u64>()?,
                                    )
                                } else {
                                    panic!("Congrats, you broke the regex")
                                }
                            }
                        };

                        query.included_bot_params.sort = true;
                    }
                }

                // Remove prefix
                query.query = String::from(
                    query
                        .orginal_query
                        .strip_prefix(bot_params_raw.get(0).unwrap().as_str())
                        .unwrap(),
                );
            } else {
                query.query = query.orginal_query.clone();
            }
        }

        // Set filter_id
        //   from filter_id
        if filter_id.is_some() {
            query.filter_id = filter_id.unwrap();
        }
        //   from bot_params
        if query.included_bot_params.filter_id {
            query.filter_id = bot_params.filter_id;
        }

        // Set page
        //   from bot_params
        if query.included_bot_params.page {
            query.page = bot_params.page;
        }
        //   from offset
        if let Some(page) = re_offset_page.captures(offset) {
            query.page = page.get(1).unwrap().as_str().parse::<u64>()?;
        }

        // Set sorting_field
        //   from bot_params
        query.sorting_field = bot_params.sorting_field;

        // Set sorting_direction
        //   from bot_params
        query.sorting_direction = bot_params.sorting_direction;

        Ok(query)
    }

    fn get_query(&self, page: Option<bool>, filter_id: Option<bool>, sort: Option<bool>) -> String {
        let mut params: Vec<String> = vec![];
        if ((page.is_some() && page.unwrap()) || self.included_bot_params.page)
            && !(page.is_some() && !page.unwrap())
        {
            params.append(&mut vec![format!("page:{}", self.page)]);
        }
        if ((filter_id.is_some() && filter_id.unwrap()) || self.included_bot_params.filter_id)
            && !(filter_id.is_some() && !filter_id.unwrap())
        {
            params.append(&mut vec![format!("filter_id:{}", self.filter_id)]);
        }
        if ((sort.is_some() && sort.unwrap()) || self.included_bot_params.sort)
            && !(sort.is_some() && !sort.unwrap())
        {
            params.append(&mut vec![format!(
                "sort:{}{}",
                {
                    if matches!(self.sorting_direction, SortingDirection::Descending) {
                        ""
                    } else {
                        "-"
                    }
                },
                self.sorting_field.as_string()
            )]);
        }

        if params.len() > 0 {
            format!("bot_params:{{{}}}, {}", params.join(", "), self.query)
        } else {
            self.query.clone()
        }
    }
}

#[derive(Clone, Debug)]
struct FilterId(u64);

impl kv::Value for FilterId {
    fn to_raw_value(&self) -> Result<kv::Raw, kv::Error> {
        Ok(Into::<u64>::into(self.clone())
            .to_be_bytes()
            .to_vec()
            .into())
    }

    fn from_raw_value(r: kv::Raw) -> Result<Self, kv::Error> {
        Ok(Into::<Self>::into(u64::from_be_bytes(
            r.to_vec().try_into().map_err(|_| {
                kv::Error::Message(String::from("Error converting vec<u8> to [u8, 8]"))
            })?,
        )))
    }
}

impl Into<u64> for FilterId {
    fn into(self) -> u64 {
        let Self(int) = self;
        int
    }
}

impl From<u64> for FilterId {
    fn from(f: u64) -> Self {
        Self(f)
    }
}

#[derive(Clone, Debug)]
struct UserId(u64);

impl Into<u64> for UserId {
    fn into(self) -> u64 {
        let Self(int) = self;
        int
    }
}

impl From<u64> for UserId {
    fn from(f: u64) -> Self {
        Self(f)
    }
}

impl TryFrom<telegram_bot::UserId> for UserId {
    type Error = Error;

    fn try_from(f: telegram_bot::UserId) -> Result<Self, Error> {
        Ok(Self(Into::<i64>::into(f).try_into()?))
    }
}

impl Into<kv::Integer> for UserId {
    fn into(self) -> kv::Integer {
        Into::<u64>::into(self).into()
    }
}

impl From<kv::Integer> for UserId {
    fn from(f: kv::Integer) -> Self {
        Self(f.into())
    }
}

type QueryCache = Arc<RwLock<HashMap<Arc<Query>, Arc<ImagesResponse>>>>;

#[tokio::main]
async fn main() -> Result<(), Error> {
    let config_name = env::var("CONFIG_NAME").unwrap_or(String::from(CONFIG_NAME_DEFAULT));
    let config_dir = env::var("CONFIG_DIR").unwrap_or(String::from(CONFIG_DIR_DEFAULT));

    let config: Arc<Config> = Arc::new(
        confy::load_path(
            &Path::new(&config_dir)
                .join(APP_NAME)
                .join(&format!("{}.toml", config_name)),
        )
        .unwrap(),
    );

    pretty_env_logger::formatted_builder()
        .parse_filters(&config.log_level)
        .init();

    debug!("{:#?}", config);

    let db = kv::Store::new(kv::Config::new(&config.db_path)).unwrap();
    let user_id_to_filter_id = db.bucket::<kv::Integer, FilterId>(Some("testing")).unwrap();

    let query_cache = QueryCache::default();

    let rate_limiter = Arc::new(RateLimiter::direct(Quota::per_second(nonzero!(2u32))));

    let api = Arc::new(RwLock::new(Api::new(&config.telegram_token)));
    let mut stream = api.read().await.stream();

    while let Some(update) = stream.next().await {
        let api = api.clone();
        let config = config.clone();
        let query_cache = query_cache.clone();
        let user_id_to_filter_id = user_id_to_filter_id.clone();
        let rate_limiter = rate_limiter.clone();

        spawn(async move {
            (async {
            let update: Update = update?;
            debug!("Update: {:#?}", update);

            if let UpdateKind::Message(message) = update.kind {
                if let MessageKind::Text { ref data, .. } = message.kind {
                    info!("<{}>: {}", &message.from.first_name, data);

                    if data.starts_with("/forget_me") {
                        let filter_id_old = &user_id_to_filter_id
                            .get(TryInto::<UserId>::try_into(message.from.id)?)
                            .unwrap();

                        &user_id_to_filter_id
                            .remove(TryInto::<UserId>::try_into(message.from.id)?)
                            .unwrap();
                        api.write()
                                    .await
                                    .send(
                                        message.chat.text(
                                            match filter_id_old {
                                                    Some(filter_id) => format!("I removed any data I have about you. Your filter id was set to {} before.", Into::<u64>::into(filter_id.clone())),
                                                    None => format!("I don't know anything about you anyways."),
                                            }).parse_mode(ParseMode::Markdown)
                                    )
                                    .await?;
                    } else if data.starts_with("/filter_id") {
                        let filter_id: Result<u64, Error> = (|| {
                            Ok(data
                                .split(" ")
                                .nth(1)
                                .ok_or_else(|| format_err!("Argument missing"))?
                                .parse()?)
                        })();

                        match filter_id {
                            Ok(filter_id) => {
                                user_id_to_filter_id
                                    .set(
                                        TryInto::<UserId>::try_into(message.from.id)?,
                                        FilterId(filter_id),
                                    )
                                    .unwrap();

                                api.write()
                                    .await
                                    .send(
                                        message.chat.text(format!("Your filter id is now set to {}. (The default is {})\nIf you want me to forget your preferences use /forget\\_me", filter_id, config.default_filter)).parse_mode(ParseMode::Markdown)
                                    )
                                    .await?;

                                user_id_to_filter_id.flush_async().await?;
                            }
                            Err(_) => {
                                api.write()
                                    .await
                                    .send(
                                        message.chat.text(format!("{}. (The default is {})\nUse `/filter_id <filter_id>` (Without the `<>`) to set your filter. You can find the id of a filter when viewing the filter in the url.\nIf you want me to forget your preferences use /forget\\_me", {
                                            match &user_id_to_filter_id
                                                .get(TryInto::<UserId>::try_into(message.from.id)?)
                                                .unwrap() {
                                                    Some(filter_id) => format!("Your filter id set to {}", Into::<u64>::into(filter_id.clone())),
                                                    None => format!("Your filter id is not set"),
                                            }
                                        }, config.default_filter)).parse_mode(ParseMode::Markdown)
                                    )
                                    .await?;
                            }
                        }
                    } else if data.starts_with("/start") {
                        let url: Url = config.philomena_url.parse().unwrap();
                        let url_git: Url = config.git_url.parse().unwrap();

                        api.write()
                            .await
                            .send(
                                message.chat.text(format!("Hi, I'm an inline search bot for [{}]({}).\nMy master is @{}.\nI woun't save any data about you, unless you set your filter id with /filter\\_id.\nMy source code is available at [{}]({}).\n\nThe search works as normal with one small quirk: You can write `bot_params:{{}}, ` in front of your query to use a custom sorting order (and direction), page or filter id.\nThe `page` parameter accepts the page as a number.\nThe `filter_id` parameter accepts the filter id as a number (see the number in the url when viewing a filter).\nThe `sort` parameter accepts one of `id`, `updated_at`, `first_seen_at`, `aspect_ratio`, `faves`, `score`, `_score`, `width`, `height`, `comment_count`, `tag_count`, `pixels`, `size`, `duration`, `random`, `random:<int>` and if you prefix the value with `-` the direction is changed.\nSo an example query could be: `bot_params:{{page:42,filter_id:1,sort:-id}}, character:derpy hooves`.\n\nHave fun!", url.host().unwrap(), url, &config.telegram_owner_name, url_git.path().strip_prefix("/").unwrap(), url_git)).parse_mode(ParseMode::Markdown).reply_markup({
                                    let mut keyboard = InlineKeyboardMarkup::new();
                                    keyboard.add_row(vec![
                                        InlineKeyboardButton::switch_inline_query_current_chat(
                                            String::from("open inline search"),
                                            String::from(""),
                                        ),
                                    ]);
                                    keyboard
                                })
                            )
                            .await?;
                    }
                }
            } else if let UpdateKind::InlineQuery(query) = update.kind {
                info!(
                    "<{}>: {:?} ({:?})",
                    &query.from.first_name, &query.query, &query.offset
                );

                match Query::parse(
                    &query.query,
                    &query.offset,
                    Some(
                        match &user_id_to_filter_id
                            .get(TryInto::<UserId>::try_into(query.from.id)?)
                            .unwrap()
                        {
                            Some(custom_filter_id) => custom_filter_id.clone().into(),
                            None => config.default_filter,
                        },
                    ),
                ) {
                    Ok(full_query) => {
                        let full_query = Arc::new(full_query);

                        match timeout(
                            Duration::from_millis(4500),
                            async || -> Result<(Arc<ImagesResponse>, bool), Error> {
                                // The bool indicates if the result comes from the cache
                                if let Some(cached_result) =
                                    query_cache.read().await.get(&full_query)
                                {
                                    return Ok((cached_result.clone(), true));
                                }

                                rate_limiter.until_ready().await;

                                let mut url: Url = config.philomena_url.parse().unwrap();
                                url.set_path("/api/v1/json/search/images");
                                url.query_pairs_mut()
                                    .clear()
                                    .append_pair("q", &full_query.query)
                                    .append_pair("page", &full_query.page.to_string())
                                    .append_pair("filter_id", &full_query.filter_id.to_string())
                                    .append_pair("sf", &full_query.sorting_field.as_string())
                                    .append_pair("sd", &full_query.sorting_direction.as_string())
                                    .append_pair("per_page", "50");

                                debug!("Requesting: {:?}", url);

                                let res = Client::builder()
                                    .build::<_, hyper::Body>(HttpsConnector::new())
                                    .get(url.as_str().parse().unwrap())
                                    .await?;

                                debug!("Received: {}", &res.status());

                                let body = hyper::body::aggregate(res).await?;
                                Ok((Arc::new(serde_json::from_reader(body.reader())?), false))
                            }(),
                        )
                        .await
                        {
                            Ok(data) => {
                                let (data, cached) = data?;

                                if !cached {
                                    query_cache
                                        .write()
                                        .await
                                        .insert(full_query.clone(), data.clone());

                                    {
                                        let query_cache = query_cache.clone();
                                        let full_query = full_query.clone();
                                        let config = config.clone();
                                        spawn((async move || {
                                            // TODO replace with https://docs.rs/tokio/0.2.22/tokio/time/delay_queue/struct.DelayQueue.html ?
                                            delay_for(Duration::from_millis(
                                                config.cache_time as u64,
                                            ))
                                            .await;

                                            query_cache.write().await.remove(&full_query);
                                        })());
                                    };
                                }

                                let mut answer = requests::AnswerInlineQuery::new(query.id, vec![]);
                                answer
                                    .cache_time(config.telegram_cache_time as i64)
                                    .next_offset(format!("page:{}", full_query.page + 1))
                                    .switch_pm_parameter(String::from("about"))
                                    .switch_pm_text(String::from("About"));

                                if full_query.filter_id != config.default_filter {
                                    answer.is_personal();
                                }

                                for img in &data.images {
                                    let mut url: Url = config.philomena_url.parse().unwrap();
                                    url.set_path(&format!("/images/{}", &img.id));
                                    let mut url_no_path = url.clone();
                                    url_no_path.set_path("/");

                                    let reply_markup = {
                                        let mut keyboard = InlineKeyboardMarkup::new();
                                        keyboard.add_row(vec![
                                            InlineKeyboardButton::switch_inline_query_current_chat(
                                                String::from("open search"),
                                                String::from(&full_query.get_query(
                                                    Some(false),
                                                    None,
                                                    None,
                                                )),
                                            ),
                                            InlineKeyboardButton::switch_inline_query_current_chat(
                                                String::from("search with page"),
                                                String::from(&full_query.get_query(
                                                    Some(true),
                                                    None,
                                                    None,
                                                )),
                                            ),
                                        ]);
                                        keyboard
                                    };

                                    let caption = format!(
                                        "[Image {}]({})\n{}([{}]({}))",
                                        &img.id,
                                        url,
                                        {
                                            let artists: Vec<String> = img
                                                .tags
                                                .iter()
                                                .map(|tag| (tag, tag.strip_prefix("artist:")))
                                                .filter(|tag| tag.1.is_some())
                                                .take(5) // TG dislikes long captions, so we have to truncate the artists
                                                .scan(0, |count, tag| {
                                                    *count += 1;

                                                    if *count < 5 {
                                                        let mut artist_url = config
                                                            .philomena_url
                                                            .parse::<Url>()
                                                            .unwrap()
                                                            .join("search")
                                                            .unwrap();
                                                        artist_url
                                                            .query_pairs_mut()
                                                            .append_pair("q", tag.0);

                                                        Some(format!(
                                                            "[{}]({})",
                                                            tag.1.unwrap(),
                                                            artist_url
                                                        ))
                                                    } else {
                                                        Some(String::from("..."))
                                                    }
                                                })
                                                .collect::<Vec<String>>();

                                            if artists.len() > 0 {
                                                format!("by {}\n", artists.join(", "))
                                            } else {
                                                String::from("")
                                            }
                                        },
                                        url.host().unwrap(),
                                        url_no_path,
                                    );

                                    answer.add_inline_result(match img.mime_type.as_ref() {
                                    "image/jpeg" | "image/png" | "image/svg+xml" => {
                                        InlineQueryResult::InlineQueryResultPhoto(
                                            InlineQueryResultPhoto {
                                                id: img.id.to_string(),
                                                photo_url: String::from(&img.representations.large),
                                                thumb_url: String::from(&img.representations.thumb),
                                                photo_width: Some(img.width),
                                                photo_height: Some(img.height),
                                                title: Some(String::from(&img.name)),
                                                description: Some(String::from(&img.description)),
                                                caption: Some(caption),
                                                parse_mode: Some(ParseMode::Markdown),
                                                reply_markup: Some(reply_markup),
                                                input_message_content: None,
                                            },
                                        )
                                    }
                                    "image/gif" => {
                                        InlineQueryResult::InlineQueryResultGif(InlineQueryResultGif {
                                            id: img.id.to_string(),
                                            gif_url: String::from(&img.representations.large),
                                            thumb_url: String::from(&img.representations.thumb),
                                            gif_width: Some(img.width),
                                            gif_height: Some(img.height),
                                            gif_duration: img.duration.map(|duration| duration as i64),
                                            title: Some(String::from(&img.name)),
                                            caption: Some(caption),
                                            parse_mode: Some(ParseMode::Markdown),
                                            reply_markup: Some(reply_markup),
                                            input_message_content: None,
                                        })
                                    }
                                    "video/webm" => InlineQueryResult::InlineQueryResultVideo(
                                        InlineQueryResultVideo {
                                            id: img.id.to_string(),
                                            video_url: String::from(
                                                img.representations
                                                    .large
                                                    .parse::<Url>()?
                                                    .join(&config.video_file_name)?
                                                    .as_str(),
                                            ),
                                            thumb_url: String::from(&config.play_button_image_url),
                                            video_width: Some(img.width),
                                            video_height: Some(img.height),
                                            video_duration: img
                                                .duration
                                                .map(|duration| duration as i64),
                                            mime_type: String::from("video/mp4"),
                                            title: String::from(&img.name),
                                            description: Some(String::from(&img.description)),
                                            caption: Some(caption),
                                            parse_mode: Some(ParseMode::Markdown),
                                            reply_markup: Some(reply_markup),
                                            input_message_content: None,
                                        },
                                    ),
                                    _ => {
                                        let error = format!(
                                            "Unhandeled mime_type {:?}, post: {}",
                                            img.mime_type, url
                                        );

                                        warn!("{}", error);

                                        InlineQueryResult::InlineQueryResultPhoto(
                                            InlineQueryResultPhoto {
                                                id: format!("e_mime_{}", img.id),
                                                photo_url: String::from(&config.placeholder_image_url),
                                                thumb_url: String::from(&config.placeholder_image_url),
                                                photo_width: None,
                                                photo_height: None,
                                                title: None,
                                                description: None,
                                                caption: Some(error.clone()),
                                                parse_mode: Some(ParseMode::Markdown),
                                                reply_markup: Some({
                                                    let mut keyboard = InlineKeyboardMarkup::new();
                                                    keyboard.add_row(vec![
                                                InlineKeyboardButton::switch_inline_query_current_chat(
                                                    String::from("retry search"),
                                                    String::from(&full_query.orginal_query),
                                                ),
                                            ]);
                                                    keyboard
                                                }),
                                                input_message_content: Some(
                                                    InputMessageContent::InputTextMessageContent(
                                                        InputTextMessageContent {
                                                            message_text: error,
                                                            parse_mode: Some(ParseMode::Markdown),
                                                            disable_web_page_preview: false,
                                                        },
                                                    ),
                                                ),
                                            },
                                        )
                                    }
                                });
                                }

                                api.write().await.send(answer).await?;
                            }
                            Err(_) => {
                                // Timed out (or rate limiter lead to time out)

                                let error = String::from("Connection timeout out or rate limit was reached");
                                warn!("{}", &error);

                                let mut answer = requests::AnswerInlineQuery::new(query.id, vec![]);
                                answer
                                    .cache_time(0)
                                    .is_personal()
                                    .next_offset(format!("{}_", &query.offset))
                                    .add_inline_result(InlineQueryResultPhoto {
                                        id: String::from("e_time"),
                                        photo_url: String::from(&config.placeholder_image_url),
                                        thumb_url: String::from(&config.placeholder_image_url),
                                        photo_width: None,
                                        photo_height: None,
                                        title: None,
                                        description: None,
                                        caption: Some(error.clone()),
                                        parse_mode: Some(ParseMode::Markdown),
                                        reply_markup: Some({
                                            let mut keyboard = InlineKeyboardMarkup::new();
                                            keyboard.add_row(vec![
                                            InlineKeyboardButton::switch_inline_query_current_chat(
                                                String::from("retry search"),
                                                String::from(&full_query.orginal_query),
                                            ),
                                        ]);
                                            keyboard
                                        }),
                                        input_message_content: Some(
                                            InputMessageContent::InputTextMessageContent(
                                                InputTextMessageContent {
                                                    message_text: error.clone(),
                                                    parse_mode: Some(ParseMode::Markdown),
                                                    disable_web_page_preview: false,
                                                },
                                            ),
                                        ),
                                    });

                                api.write().await.send(answer).await?;
                            }
                        };
                    }
                    Err(e) => {
                        let error_string = format!("Error parsing query and offset: {}", e);

                        error!("{}", error_string);

                        let mut answer = requests::AnswerInlineQuery::new(query.id, vec![]);
                        answer.cache_time(0).is_personal().add_inline_result(
                            InlineQueryResultPhoto {
                                id: String::from("e_parse"),
                                photo_url: String::from(&config.placeholder_image_url),
                                thumb_url: String::from(&config.placeholder_image_url),
                                photo_width: None,
                                photo_height: None,
                                title: None,
                                description: None,
                                caption: Some(error_string.clone()),
                                parse_mode: Some(ParseMode::Markdown),
                                reply_markup: Some({
                                    let mut keyboard = InlineKeyboardMarkup::new();
                                    keyboard.add_row(vec![
                                        InlineKeyboardButton::switch_inline_query_current_chat(
                                            String::from("retry search"),
                                            String::from(&query.query),
                                        ),
                                    ]);
                                    keyboard
                                }),
                                input_message_content: Some(
                                    InputMessageContent::InputTextMessageContent(
                                        InputTextMessageContent {
                                            message_text: error_string,
                                            parse_mode: Some(ParseMode::Markdown),
                                            disable_web_page_preview: false,
                                        },
                                    ),
                                ),
                            },
                        );

                        api.write().await.send(answer).await?;
                    }
                }
            }

            Ok::<(), Error>(()) // Workaround https://github.com/rust-lang/rust/issues/63502
        }).await.unwrap();
        });
    }

    Ok(())
}
